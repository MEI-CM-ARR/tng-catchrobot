module.exports = (sequelize, DataTypes) => {
    return sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        password: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: ""
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        companyId: {
            type: DataTypes.INTEGER
        }
    }, {
        timestamps: true,
        underscored: true,
        paranoid: true,
        freezeTableName: true,
        tableName: 'user'
    });
};