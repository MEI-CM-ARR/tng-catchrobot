module.exports = (sequelize, DataTypes) => {
    return sequelize.define('UserToken', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        userId: {
            type: DataTypes.INTEGER,
            references: {
                model: sequelize.import(__dirname + '/user'),
                key: 'id'
            },
            allowNull: true,
            field: 'user_id'
        },
        token: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        timestamps: true,
        underscored: true,
        paranoid: true,
        freezeTableName: true,
        tableName: 'user_token'
    });
};