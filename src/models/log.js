module.exports = (sequelize, DataTypes) => {
    return sequelize.define('Log', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        companyId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        robotId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        eventType: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 0
        }
    }, {
        timestamps: true,
        underscored: true,
        paranoid: true,
        freezeTableName: true,
        tableName: 'log'
    });
};
