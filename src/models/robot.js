module.exports = (sequelize, DataTypes) => {
    return sequelize.define('Robot', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        publicKey: {
            type: DataTypes.TEXT,
            allowNull: false,
            field: 'public_key'
        },
        mac: {
            type: DataTypes.STRING,
            unique: true
        },
        companyId: {
            type: DataTypes.INTEGER,
            references: {
                model: sequelize.import(__dirname + '/company'),
                key: 'id'
            },
            allowNull: true,
            field: 'company_id'
        },
        socketId: {
            type: DataTypes.STRING,
            allowNull: null,
            field: 'socket_id'
        },
        localIp: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'local_ip'
        },
        inManualControl: {
            type: DataTypes.BOOLEAN,
            allowNull: null,
            defaultValue: false
        },
        state: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        target: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        timestamps: true,
        underscored: true,
        paranoid: true,
        freezeTableName: true,
        tableName: 'robot'
    });
};