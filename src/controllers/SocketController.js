const socketio = require("socket.io");

let Response = require("../helpers/Response");
let EventType = require("../helpers/EventType");

class SocketController {
    constructor(
        server,
        authController,
        userController,
        robotController,
        companyController,
        detectionController,
        eventController
    ) {
        this.server = server;
        this.authController = authController;
        this.userController = userController;
        this.robotController = robotController;
        this.companyController = companyController;
        this.detectionController = detectionController;
        this.eventController = eventController;

        this.connect();
    }

    getSocketById(socketId) {
        let socket = this.io.sockets.connected[socketId];
        if (!socket) {
            return false;
        }
        return socket;
    }

    connect() {
        this.io = socketio(this.server);

        this.io.on("connect", clientSocket => {
            console.log("Connected");
            /**
             *
             * Global linking
             *
             */

            clientSocket.on("user:create", async userInfo => {
                // TODO: Add fail response
                let company = await this.companyController.getByName(
                    userInfo.company
                );
                let companyId = null;
                if (company) {
                    companyId = company.id;
                } else {
                    companyId = await this.companyController.add(
                        userInfo.company
                    );
                }
                let newUserId = await this.userController.add(
                    userInfo.email,
                    userInfo.password,
                    companyId
                );
                let userToken = await this.authController.login(
                    userInfo.email,
                    userInfo.password
                );

                clientSocket.emit(
                    "user:create:response",
                    Response.success(userToken)
                );
                // We are sure it's a user
                this.initUserSocket(clientSocket, newUserId);
            });

            clientSocket.on("auth:user:token", async data => {
                try {
                    //console.log(data.token);
                    let user = await this.authController.tokenLogin(data.token);

                    clientSocket.on("auth:user:logout", async () => {
                        await this.authController.logout(user.email);
                    });

                    // TODO: Do we send user data back or not?
                    this.initUserSocket(clientSocket, user.id);

                    clientSocket.emit(
                        "auth:user:token:response",
                        Response.success({ message: "Logged with success" })
                    );
                } catch (err) {
                    clientSocket.emit(
                        "auth:user:token:response",
                        Response.error("Invalid token")
                    );
                }
            });

            clientSocket.on("auth:user:login", async data => {
                try {
                    let result = await this.authController.login(
                        data.email,
                        data.password
                    );
                    let response;

                    if (!result) {
                        response = Response.error("Invalid login");
                        clientSocket.emit("auth:user:login:response", response);
                        return;
                    }

                    clientSocket.on("auth:user:logout", async () => {
                        await this.authController.logout(result.user.email);
                    });

                    this.initUserSocket(clientSocket, result.user.id);
                    clientSocket.emit(
                        "auth:user:login:response",
                        Response.success({ token: result.token })
                    );
                } catch (e) {
                    clientSocket.emit(
                        "auth:user:login:response",
                        Response.error({ message: e.message })
                    );
                }
            });

            clientSocket.on("auth:robot", async authParameters => {
                let robotId = await this.authController.robotLogin(
                    authParameters,
                    clientSocket.id
                );
                console.log("[SoketController][AuthRobot]: Robot login");
                // TODO: add validation?

                let robot = await this.robotController.getById(robotId);

                if (robot.companyId) {
                    this.eventController.logNewEvent(
                        robot.companyId,
                        robot.id,
                        EventType.CameOnline
                    );
                }
                if (robot.companyId) {
                    this.io.to(robot.companyId + "/users").emit("update");
                }
                this.initRobotSocket(clientSocket, robotId);
                clientSocket.emit("auth:robot:response", Response.success());
            });
        });
    }

    initUserSocket(userSocket, userId) {
        this.userController
            .getById(userId)
            .then(user => {
                if (!user.companyId) {
                    console.error(
                        "I(user) cannot join to my room because I not have a company id"
                    );
                }
                userSocket.companyId = user.companyId;
                userSocket.join(userSocket.companyId + "/users");
            })
            .catch(e => {
                console.error(e);
            });

        userSocket.on("disconnect", () => {
            userSocket.disconnect(false);
        });

        userSocket.on("robots:get", async () => {
            let user = await this.userController.getById(userId);
            let robots = await this.robotController.getCompanyRobots(
                user.companyId
            );
            userSocket.emit("robots:response", Response.success(robots));
        });

        userSocket.on("user:action", comand => {
            if (userSocket.robotSocket) {
                userSocket.robotSocket.emit("user:action", comand);
            }
        });

        userSocket.on("user:addRobot", async data => {
            console.log("Adding new robot");
            let robot = await this.robotController.getByMac(data.mac);
            if (!robot) {
                userSocket.emit(
                    "user:AddRobot:response",
                    Response.error({
                        message: "Requested robot does not exist"
                    })
                );
                return;
            }
            if (robot.companyId) {
                userSocket.emit(
                    "user:AddRobot:response",
                    Response.error({ message: "Invalid request" })
                );
                return;
            }
            let user = await this.userController.getById(userId);
            await this.robotController.associateRobotToCompany(
                robot.id,
                user.companyId
            );
            userSocket.emit(
                "user:AddRobot:response",
                Response.success({ robotId: robot.id })
            );
            let robots = await this.robotController.getCompanyRobots(
                user.companyId
            );

            this.eventController.logNewEvent(
                user.companyId,
                robot.id,
                EventType.NewRobot
            );
            this.io.to(robot.companyId + "/users").emit("update");
            userSocket.emit("robots:response", Response.success(robots));
        });

        userSocket.on("robot:changeState", async data => {
            console.log("[SocketController][change robot state]");
            await this.robotController.setRobotState(
                data.robot.id,
                data.robot.state
            );
            let robot = await this.robotController.getById(data.robot.id);

            if (!robot) {
                userSocket.emit(
                    "robot:changeState:response",
                    Response.error({ message: "Robot not found" })
                );
                return;
            }

            if (!robot.socketId) {
                userSocket.emit(
                    "robot:changeState:response",
                    Response.error({ message: "Robot has no socket" })
                );
                return;
            }

            let robotSocket = this.getSocketById(robot.socketId);
            console.log("Socket is saved: " + robotSocket != null);
            if (robotSocket) {
                console.log(
                    "[User controller][emit robot state]: Emiting to robot its new state"
                );
                robotSocket.emit(
                    data.robot.state ? "robot:activate" : "robot:deactivate"
                );
            } else {
                userSocket.emit(
                    "robot:changeState:response",
                    Response.error({ message: "Robot not connected" })
                );
                return;
            }
            let user = await this.userController.getById(userId);
            this.eventController.logNewEvent(
                user.companyId,
                robot.id,
                EventType.StateChange
            );
            userSocket.emit(
                "robot:changeState:response",
                Response.success({ message: "Robot state set successful" })
            );
            this.io.to(robot.companyId + "/users").emit("update");
        });

        userSocket.on("manual-control", async (robotId, inManualControl) => {
            inManualControl = !!inManualControl; //forcing to be boolean

            console.log("[SocketController] [manual-control]");
            let robot = await this.robotController.getById(robotId);

            if (!robot) {
                userSocket.emit(
                    "manual:control:response",
                    Response.error({ message: "Invalid robot Id" })
                );
                return;
            }

            if (!robot.socketId) {
                userSocket.emit(
                    "manual:control:response",
                    Response.error({ message: "Robot not connected" })
                );
                return;
            }

            let robotSocket = this.getSocketById(robot.socketId);
            if (!robotSocket) {
                userSocket.emit(
                    "manual:control:response",
                    Response.error({ message: "Robot not connected" })
                );
                return;
            }

            if (inManualControl && robot.inManualControl) {
                if (userSocket == robot.socketId) {
                    //TODO - Check this
                    userSocket.emit(
                        "manual:control:response",
                        Response.error({
                            message:
                                "This robot is already being controlled by you"
                        })
                    );
                } else {
                    userSocket.emit(
                        "manual:control:response",
                        Response.error({
                            message:
                                "This robot is already being controlled by other person"
                        })
                    );
                }
                return;
            }

            userSocket.robotSocket = robotSocket;
            robotSocket.userSocket = userSocket;
            robotSocket.emit("manual-control", inManualControl);
            userSocket.emit("manual:control:response", {
                robotID: robot.id,
                isInManualControl: inManualControl
            });

            this.eventController.logNewEvent(
                robot.companyId,
                robot.id,
                EventType.ManualControl
            );
            await this.robotController.setRobotInManualControl(
                robot.id,
                inManualControl
            );

            this.io.to(robot.companyId + "/users").emit("update");
        });

        userSocket.on("user:target", (imageBuffer, ack) => {
            this.detectionController.processImage(
                imageBuffer,
                userSocket,
                ack,
                null
            );
        });

        userSocket.on("user:set:target", async data => {
            // set target to robot
            console.log("[SocketController][setTarget]: " + data.label);
            this.robotController.setTargetToRobot(data.robotId, data.label);
            let user = await this.userController.getById(userId);
            this.eventController.logNewEvent(
                user.companyId,
                robot.id,
                EventType.NewTarget
            );
        });

        userSocket.on("dashboard:update", async () => {
            let user = await this.userController.getById(userId);

            var data = {};

            data[
                "auto_now"
            ] = await this.robotController.getNumberOfRobotsInAutomaticMode(
                user.companyId
            );
            data[
                "robots"
            ] = await this.robotController.getNumberOfCompanyRobots(
                user.companyId
            );
            data[
                "manual_now"
            ] = await this.robotController.getNumberOfRobotsInManualControl(
                user.companyId
            );
            data[
                "connected_now"
            ] = await this.robotController.getNumberOfConnectedRobots(
                user.companyId
            );

            data[
                "online_day"
            ] = await this.eventController.getNumberOfConnectionsPerDay(
                user.companyId
            );
            data[
                "manual_day"
            ] = await this.eventController.getNumberOfManualControlsPerDay(
                user.companyId
            );
            data[
                "state_day"
            ] = await this.eventController.getNumberOfStateChangesPerDay(
                user.companyId
            );

            userSocket.emit(
                "dashboard:update:response",
                Response.success(data)
            );
        });
    }

    initRobotSocket(robotSocket, robotId) {
        this.robotController
            .getById(robotId)
            .then(robot => {
                if (!robot.companyId) {
                    console.error(
                        "I(robot) cannot join to my room because I not have a company id"
                    );
                }
                robotSocket.companyId = robot.companyId;
                robotSocket.join(robotSocket.companyId + "/robots");
            })
            .catch(e => {
                console.error(e);
            });

        robotSocket.on("disconnect", async () => {
            this.robotController.clearSocket(robotId);
            robotSocket.disconnect(false);
            let robot = await this.robotController.getById(robotId);
            this.io.to(robot.companyId + "/users").emit("update");
        });

        robotSocket.on("robot:image:new", async (image, ack) => {
            console.log("[SocketController] [image:new]");
            let robot = await this.robotController.getById(robotId);
            await this.detectionController.processImage(
                image,
                robotSocket,
                ack,
                robot.target
            );
        });

        robotSocket.on("robot:getState", async () => {
            let robotState = await this.robotController.getRobotState(robotId);
            robotSocket.emit("robot:state", robotState.state);
        });

        robotSocket.on("robot:new_frame", (imageBase64, ack) => {
            if (!robotSocket.userSocket || !robotSocket.userSocket.connected) {
                console.error("robot don't know the user controller");
                return;
            }
            robotSocket.userSocket.emit("robot:new_frame", imageBase64);
            ack();
        });

        robotSocket.on("notification", notification => {
            if (!robotSocket.companyId) {
                console.error("I cannot emit because I not have a companyID");
                return;
            }
            this.io
                .to(robotSocket.companyId + "/users")
                .emit("notification", notification);
        });
    }
}

module.exports = SocketController;
