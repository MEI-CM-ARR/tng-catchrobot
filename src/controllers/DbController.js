const models = require("./../models/index.js");
const Sequelize = require("sequelize");
const Op = Sequelize.Op


class DbController {
    constructor() {
        Object.assign(this, models);
        this.Company.hasMany(this.Robot, { as: "Robots" });
    }

    async sync() {
        await this.sequelize.sync().then(() => {
            console.log(`Database & tables created!`);
        });
    }

    async createDummyData(userController, companyController, robotController) {
        /*
        let nrOfCompanies = await this.Company.count();
        if (nrOfCompanies != 0) {
            return;
        }

        let companyId = await companyController.add("Test Company, LDA");
        await userController.add("test@test.com", "test", companyId);

        let res = await robotController.add({publicKey: "abcdefgh", mac: "ab:ab:ab:ab:ab:ab"}, "NOID", 1);
        //console.log(res);
        */
    }
}

module.exports = DbController;
