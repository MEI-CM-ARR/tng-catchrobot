let EventType = require("../helpers/EventType");

class EventController {
    constructor(dbController) {
        this.dbController = dbController;
    }

    // TODO: Vale a pena um controller so para esta funcao?
    async logNewEvent(companyId, robotId, eventType) {
        await this.dbController.Log.create({
            companyId: companyId,
            robotId: robotId,
            eventType: eventType
        });
    }

    async getNumberOfConnectionsPerDay(companyId) {
        let result = await this.dbController.Log.findAndCountAll({
            attributes: ["created_at"],
            where: {
                companyId: companyId,
                eventType: EventType.CameOnline
            },
            group: [
                this.dbController.sequelize.fn(
                    "day",
                    this.dbController.sequelize.col("created_at")
                )
            ]
        });

        return result;
    }

    async getNumberOfManualControlsPerDay(companyId) {
        let result = await this.dbController.Log.findAndCountAll({
            attributes: ["created_at"],
            where: {
                companyId: companyId,
                eventType: EventType.ManualControl
            },
            group: [
                this.dbController.sequelize.fn(
                    "day",
                    this.dbController.sequelize.col("created_at")
                )
            ]
        });

        return result;
    }

    async getNumberOfStateChangesPerDay(companyId) {
        let result = await this.dbController.Log.findAndCountAll({
            attributes: ["created_at"],
            where: {
                companyId: companyId,
                eventType: EventType.StateChange
            },
            group: [
                this.dbController.sequelize.fn(
                    "day",
                    this.dbController.sequelize.col("created_at")
                )
            ]
        });

        return result;
    }
}

module.exports = EventController;
