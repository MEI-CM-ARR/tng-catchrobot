let Response = require("../helpers/Response");
const bcrypt = require("bcrypt");
const saltRounds = 10;

class UserController {
    constructor(dbController) {
        this.dbController = dbController;
    }

    async getByUsername(email) {
        let userData = await this.dbController.User.findOne({
            where: {
                email: email
            }
        });

        return userData ? userData.dataValues : null;
    }

    async getById(id) {
        return await this.dbController.User.findByPk(id);
    }

    async add(email, password, companyId) {
        let passwordHash = await bcrypt.hash(password, saltRounds);

        let result = await this.dbController.User.create({
            password: passwordHash,
            email: email,
            companyId: companyId
        });
        return result.id;
    }
}

module.exports = UserController;
