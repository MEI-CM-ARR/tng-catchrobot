const Yolo = require("@vapi/node-yolo");
const fs = require("promisify-fs");
const util = require("util");
const readdir = util.promisify(require("fs").readdir);

let Response = require("../helpers/Response");

const TMP_FOLDER_NAME = "tmp";

class DetectionController {
    constructor() {
        this.detector = new Yolo(
            "config",
            "cfg/coco.data",
            "cfg/yolov3.cfg",
            "yolov3.weights"
        );
        DetectionController.createTmpDir();
    }

    static async createTmpDir() {
        await fs.addFolder(TMP_FOLDER_NAME);
    }

    async processImage(imageBuffer,socket, ack, target = null) {
        let randomNumber = Math.floor(Math.random() * 99999);
        let filename = TMP_FOLDER_NAME + "/" + randomNumber + ".jpg";
        await fs.writeFile(filename, imageBuffer, "binary");
        console.log("[Detection][Controller]" + target);
        try {
            this.detector
                .detect(filename)
                .then(detections => {
                    if (detections && detections.length > 0) {
                        let uniqueDetections = this.getUniqueDetections(
                            detections
                        );

                        if (target) {
                            let objectToDetect = target;

                            let filterDetection = uniqueDetections
                                .find(uniqueDetection => {
                                        return objectToDetect === uniqueDetection.className;
                                    }
                                );

                            ack(Response.success(filterDetection));
                        } else {
                            let object = detections.reduce((prev, current) => (prev.probability > current.probability) ? prev : current);
                            ack(Response.success({label: object.className}));
                        }
                    } else {
                        ack(Response.success());
                    }

                    fs.delFile(filename);
                })
                .catch(error => {
                    fs.delFile(filename);
                    DetectionController.processError(error, socket);
                });
        } catch (error) {
            fs.delFile(filename);
            DetectionController.processError(error, socket);
        }
    }

    getUniqueDetections(detections) {
        let uniqueDetections = [];

        for (let detection of detections) {
            let detectionWithTheSameClassIndex = uniqueDetections.findIndex(
                uniqueDetection => {
                    return uniqueDetection.className === detection.className;
                }
            );

            let detectionWithTheSameClass =
                uniqueDetections[detectionWithTheSameClassIndex];

            if (!detectionWithTheSameClass) {
                uniqueDetections.push(detection);
            } else if (detectionWithTheSameClass.probability < detection.probability) {
                uniqueDetections[detectionWithTheSameClassIndex] = detection;
            }
        }
        return uniqueDetections;
    }

    static processError(e, socket) {
        socket.emit("image:detection:error", Response.error(e.message));
        console.error(e);
    }
}

module.exports = DetectionController;
