class CompanyController {

    constructor(dbController) {
        this.dbController = dbController;
    }

    async add(name) {
        let result = await this.dbController.Company.create({
            name
        });
        return result.id
    }

    async getByName(name) {
        let result = await this.dbController.Company.findOne({
            where: {
                name: name
            }
        });
        return result ? result.dataValues : null;
    }
}

module.exports = CompanyController;