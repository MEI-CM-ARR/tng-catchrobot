const bcrypt = require("bcrypt");

const Response = require("../helpers/Response");
const crypto = require("crypto");

class AuthController {
    constructor(dbController, userController, robotController) {
        this.dbController = dbController;
        this.userController = userController;
        this.robotController = robotController;
    }

     async robotLogin(authParameters, socketId) {
            let robot = await this.robotController.getByMac(authParameters.mac);
            if (robot) {
                let valid = this.robotController.verifySignature(
                    robot.mac,
                    authParameters.signature,
                    robot.publicKey
                );

                if (!valid) {
                    console.log("TODO: Public key not valid")
                    //socket.disconnect();
                    //return;
                    //reject();
                }

                await this.robotController.updateTempFields(robot.id, socketId, authParameters.localIp);
                return robot.id;
            } else {
                return await this.robotController.add(authParameters, socketId, null);
            }
    }

    login(email, password) {
        return new Promise(async (resolve, reject) => {
            let user = await this.userController.getByUsername(email);

            if (!user) {
                throw new Error("Invalid username");
            }

            let isValid = await bcrypt.compare(password, user.password);

            if (!isValid) {
                throw new Error("Invalid password");
            }

            crypto.randomBytes(48, async (err, buffer) => {
                if (err) {
                    reject(err);
                    return;
                }

                let token = buffer.toString("hex");

                let userToken = await this.dbController.UserToken.create({
                    userId: user.id,
                    token: token
                });

                resolve({ token: userToken, user: user});
            });
        });
    }

    tokenLogin(token) {
        return new Promise(async (resolve, reject) => {

            let result = await this.dbController.UserToken.findOne({ where: {token: token} });

            if (result) {
                let user = await this.userController.getById(result.userId);
                resolve(user);
            } else {
                reject();
            }

        });
    }

    logout(email) {
        return new Promise(async (resolve, reject) => {
            try {
                let user = await this.userController.getByUsername(email);
                await this.dbController.UserToken.destroy({
                    where: {
                        userId: user.id
                    }
                });
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }
}

module.exports = AuthController;
