const ursa = require("ursa");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

class RobotController {
    constructor(dbController) {
        this.dbController = dbController;
    }

    async getByMac(mac) {
        let result = await this.dbController.Robot.findOne({
            where: {
                mac: mac
            }
        });

        return result ? result.dataValues : null;
    }

    async getById(id) {
        let response = await this.dbController.Robot.findByPk(id);

        if (!response){
            return null;
        }

        return response.dataValues;

    }

    async getCompanyRobots(companyId) {
        return await this.dbController.Robot.findAll({
            where: { companyId: companyId }
        });
    }

    async clearSocket(robotId) {
        await this.dbController.Robot.update(
            {
                socketId: null
            },
            {
                where: {
                    id: robotId
                }
            }
        );
    }

    verifySignature(mac, signature, publicKeyBase64) {
        try {
            let buf = Buffer.from(publicKeyBase64, "base64");
            let publicKey = ursa.createPublicKey(buf);
            const verify = ursa.createVerifier("sha256");
            verify.update(mac);

            return verify.verify(publicKey, signature);
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    async updateTempFields(robotId, socketId, localIp) {
        await this.dbController.Robot.update(
            {
                socketId: socketId,
                localIp: localIp
            },
            {
                where: {
                    id: robotId
                }
            }
        );
    }

    async setRobotInManualControl(robotId, inManualControl) {
        await this.dbController.Robot.update(
            {
                inManualControl: inManualControl
            },
            {
                where: {
                    id: robotId
                }
            }
        );
    }

    async getRobotState(robotId) {
        let result = await this.dbController.Robot.findOne(
            {
                attributes: ["state"]
            },
            {
                where: {
                    id: robotId
                }
            }
        );

        return result ? result.dataValues : null;
    }

    async setRobotState(robotId, state) {
        await this.dbController.Robot.update(
            {
                state: state
            },
            {
                where: {
                    id: robotId
                }
            }
        );
    }

    async setTargetToRobot(robotId, newTarget) {
        await this.dbController.Robot.update(
            {
                target: newTarget
            },
            {
                where: {
                    id: robotId
                }
            }
        );
    }

    async add(authParameters, socketId, companyId) {
        let result = await this.dbController.Robot.create({
            publicKey: authParameters.publicKey,
            mac: authParameters.mac,
            socketId: socketId,
            companyId: companyId,
            localIp: authParameters.localIp
        });
        return result.id;
    }

    async associateRobotToCompany(robotId, companyId) {
        // TODO: What kind of validation should we do?
        let result = await this.dbController.Robot.update(
            { companyId: companyId },
            { where: { id: robotId, companyId: null } }
        );
    }

    async getNumberOfRobotsInAutomaticMode(companyId) {
        let num = await this.dbController.Robot.count({
            where: {
                companyId: companyId,
                state: true
            }
        });

        return num;
    }

    async getNumberOfCompanyRobots(companyId) {
        let num = await this.dbController.Robot.count({
            where: {
                companyId: companyId
            }
        });

        return num;
    }

    async getNumberOfRobotsInManualControl(companyId) {
        let num = await this.dbController.Robot.count({
            where: {
                inManualControl: true,
                companyId: companyId
            }
        });

        return num;
    }

    async getNumberOfConnectedRobots(companyId) {
        let num = await this.dbController.Robot.count({
            where: {
                socketId: {
                    [Op.ne]: null
                },
                companyId: companyId
            }
        });
        return num;
    }

    

    
}

module.exports = RobotController;
