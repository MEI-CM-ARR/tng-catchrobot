/*
class EventType {
    constructor() {}

    static newRobot() {
        return "NewRobot";
    }

    static newTarget() {
        return "NewTarget";
    }

    static cameOnline() {
        return "CameOnline";
    }

    static disconnected() {
        return "Disconnected";
    }

    static setAtive() {
        return "SetActive";
    }
}
*/
let EventType = Object.freeze({
    NewRobot: 1,
    NewTarget: 2,
    CameOnline: 3,
    Disconnected: 4,
    StateChange: 5,
    ManualControl: 6
});

module.exports = EventType;
