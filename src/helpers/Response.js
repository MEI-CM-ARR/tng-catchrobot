class Response {
    constructor(code, data) {
        this.code = code;
        this.data = data;
    }

    static success(data) {
        return new Response(200, data);
    }

    static error(data) {
        return new Response(400, data);
    }
}

module.exports = Response;
