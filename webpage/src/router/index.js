import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/components/Home";
import Dashboard from "@/components/dashboard/Dashboard";
import ManualControl from "@/components/robot/ManualControl";
import LinkRobot from "@/components/robot/LinkRobot";

Vue.use(VueRouter);

export default new VueRouter({
	routes: [
		{
			path: "/",
			name: "Home",
			component: Home,
			icon: "home"
		},
		{
			path: "/dashboard",
			name: "Dashboard",
			component: Dashboard,
			icon: "dashboard"
		},
		{
			path: "/manual-control",
			name: "Manual Control",
			component: ManualControl,
			icon: "games"
		},
		{
			path: "/robots",
			name: "Link new Robot",
			component: LinkRobot,
			icon: "device_hub"
		}
	]
});
