import { Line, mixins } from "vue-chartjs";
export default {
	extends: Line,
	mixins: [mixins.reactiveProp],
	props: ["chartData", "options", "color"],
	mounted() {
        console.log(this.color)
        let opt = {
            legend: {
                display: false,
                fontColor: this.color
            },
            responsive: true,
            maintainAspectRatio: false,
            showTooltips: false,
            showInlineValues: true,
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            fontColor: this.color
                        },
                        scaleLabel: {
                            display: true,
                            labelString: ""
                        },
                        ticks: {
                            fontColor: this.color
                        }
                    }
                ],
                yAxes: [
                    {
                        display: true,
                        ticks: {
                            beginAtZero: true,
                            fontColor: this.color
                        },
    
                        gridLines: {
                            fontColor: this.color
                        }
                    }
                ]
            },
        }

		this.renderChart(this.chartData, opt);
	}
};
