// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import VueSocketIO from "vue-socket.io";
import Vuetify from "vuetify";
import colors from 'vuetify/es5/util/colors';
import "vuetify/dist/vuetify.min.css";
import "material-design-icons-iconfont/dist/material-design-icons.css";

Vue.config.productionTip = false;

Vue.use(Vuetify, {
	theme: {
		primary: colors.lightGreen.lighten1,
		error: "#b71c1c"
	}
});

Vue.use(
	new VueSocketIO({
		debug: false,
		connection: process.env.SOCKET_URL
	})
);

new Vue({
	el: "#app",
	router,
	sockets: {
		connect() {
			let token = localStorage.getItem("auth:token");
			if (token) {
				this.$socket.emit("auth:user:token", { token: token });
			}
		},
		"auth:user:response": function(response) {
			if (response.code == 200) {
				localStorage.setItem("auth:token", response.data.token.token);
				//localStorage.setItem("user:data", response.user);
				this.isAuthenticated = true;
			} else {
				// Invalid token
				localStorage.removeItem("auth:token");
			}
		}
	},
	data: {
		isAuthenticated: false
	},
	created() {
		this.isAuthenticated = localStorage.getItem("auth:token") != null;
	},
	methods: {},
	components: { App },
	template: "<App/>"
});
