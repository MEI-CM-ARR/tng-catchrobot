"use strict";

const DetectionController = require("./src/controllers/DetectionController.js");
const SocketController = require("./src/controllers/SocketController");
const DbController = require("./src/controllers/DbController");

const RobotController = require("./src/controllers/RobotController");

const AuthController = require("./src/controllers/AuthController");
const CompanyController = require("./src/controllers/CompanyController");
const UserController = require("./src/controllers/UserController");
const EventController = require('./src/controllers/EventController');

let config = require("./config/config");

const path = require("path");

const express = require("express");
const http = require('http');
const https = require('https');
const greenlock = require('greenlock-express');
const app = express();
let lex;

let server;
if (config.https){
    lex = greenlock.create({
        // set to https://acme-v01.api.letsencrypt.org/directory in production
        server: 'https://acme-v02.api.letsencrypt.org/directory',
        email: 'ricardomaltez@gmail.com',
        agreeTos: true,
        configDir: __dirname + '/keys',
        approveDomains: ['catchrobot.ricardomaltez.com'],
        app: app
    });

    let redirectHttps = require('redirect-https')();
    let acmeChallengeHandler = lex.middleware(redirectHttps);
    http.createServer(acmeChallengeHandler).listen(80, function() {
        console.log("Listening for ACME http-01 challenges on", this.address());
    });

    server = https.createServer(lex.tlsOptions, app);
}else {
    server =  http.Server(app);
}

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, "/webpage/dist/")));
// Handles any requests that don't match the ones above

app.use(require("./src/routes/auth.js"));

app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname + "/webpage/dist/index.html"));
});



server.listen(config.webserverPort, () => {
    console.log("App is listening on port " + config.webserverPort);
});

const dbController = new DbController();

(async () => {
    await dbController.sync();
    // VAPI API handler
    const detectionController = new DetectionController();
    // Model controllers
    const companyController = new CompanyController(dbController);
    const robotController = new RobotController(dbController);
    const userController = new UserController(dbController);
    const authController = new AuthController(dbController, userController, robotController);
    const eventController = new EventController(dbController);
    // All socket logic
    const socketController = new SocketController(server, authController, userController, robotController, companyController, detectionController, eventController);

    await dbController.createDummyData(userController, companyController, robotController);
})();
